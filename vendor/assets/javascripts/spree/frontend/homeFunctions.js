function setupHome()
{
  $(".piattoDellaSettimana-aggiungi").on("click", clickOnAddSingleProduct);
  setupNewsletterButton();
}

function setupNewsletterButton()
{
  $(".iscrivitiOra").on("click", clickedOnSetupNewsletter);
  $(".buonoSconto input").on("focus",onFocusOnMailInput);
}

function clickedOnSetupNewsletter()
{
  checkEmail();
}


function resizeHome(width, height)
{
  var propFurgoncino = 2361/1294;
  var terzoBloccoHeight = height/10*6;
  var furgoncinoWidth = width*.9;
  var furgoncinoHeight = furgoncinoWidth/propFurgoncino;
  var furgoncinoTop = 0;
  if(width < 480)
    terzoBloccoHeight = height;
  var spaceLeft = terzoBloccoHeight - $(".home-terzoBlocco-top > div").height();
  var margin = 60;

  while(furgoncinoHeight > spaceLeft - margin*2)
  {
    furgoncinoWidth -= 10;
    furgoncinoHeight = furgoncinoWidth/propFurgoncino;
  }
  furgoncinoTop = (spaceLeft - furgoncinoHeight)*.5;

  // if(terzoBloccoHeight < 600)
  //   furgoncinoWidth = $(document).width()-100;
  $(".home .home-terzoBlocco").css("height", terzoBloccoHeight);
  $(".home .home-terzoBlocco #furgoncino").css({"width": furgoncinoWidth, "margin-top" : furgoncinoTop + "px"});
}



function checkEmail()
{
  var email = $(".buonoSconto input").val();
  if(validateEmail(email))
    sendMailForNewsletter(email);
  else
  {
    $(".buonoSconto").addClass("mailError");
  }
}

function sendMailForNewsletter(email)
{
  var ajaxCall = $.ajax({ 
    url: '/newsletter/add_to_newsletter', 
    type: 'POST', 
    dataType: 'json', 
    data : {"email" : email}
  });
  ajaxCall.done(ajaxAddSuccessNewsletter)
  .fail(ajaxAddErrorNewsletter);
}

function onFocusOnMailInput()
{
  $(this).parent().parent().removeClass("mailError");
}

function ajaxAddSuccessNewsletter(data, textStatus, jqXHR)
{
  $(".buonoSconto").removeClass("mailError");
  $(".buonoSconto").addClass("mailSended");
  $(".iscrivitiOra").off("click", clickedOnSetupNewsletter);;
  setTimeout(function()
  {
    $(".iscrivitiOra").html("Grazie per esserti registrato!");
  },200);
}

function ajaxAddErrorNewsletter(data, textStatus, jqXHR)
{  
  // console.log(data);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
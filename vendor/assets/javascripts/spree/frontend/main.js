document.addEventListener('DOMContentLoaded', onMainReady);

function onMainReady()
{
  if(!mobile)
    $('[data-toggle="tooltip"]').tooltip(); 
  if($("body").hasClass("menu"))
    setupMenu();
  if($("body").hasClass("home"))
    setupHome();


  initCookies();
  $(window).on("resize", onResize);
  setTimeout(function(){
    onResize();
  }, 100);
  onResize();
  $("#carrelloBtn > a").on("click", clickOnCarrelloBtn);
  initSearchForm();
}

function initSearchForm()
{
  $("form#formSearch").submit("submit", function(e)
  {
    if(window.location.href.indexOf("menu") != -1)
    {
      e.preventDefault();
      onSubmitSearch();
    }
  });
}

function onSubmitSearch(e)
{
  var val = $("input#search").val();
  console.log(val);
  console.log(window);
  updateFilters(val); 
}

function clickOnCarrelloBtn(e)
{
  var total = parseInt($(this).attr("data-total"));
  if(total < min_cost)
    e.preventDefault();
}

function onResize()
{
  var documentHeight = $(window).height();
  var documentWidth = $(window).width();
  if($("body").hasClass("home"))
    resizeHome(documentWidth, documentHeight);
  if($("body").hasClass("menu"))
    resizeMenu(documentWidth, documentHeight);
  setFooterPos(documentHeight);
  setTimeout(function()
  {
    setFooterPos(documentHeight);
  }, 1)
}

function setFooterPos(height)
{
  if(!$("body").hasClass("home"))
  {
    var footerHeight = $("main > footer").height() + parseFloat($("main > footer").css("padding-top")) + parseFloat($("main > footer").css("padding-bottom"));
    var mainHeight = $("main > .container").height() + parseFloat($("main > .container").css("margin-top"));
    // console.log("height = " + height);
    // console.log("footerHeight = " + footerHeight);
    // console.log("main height = " + mainHeight);
    if(mainHeight < height - footerHeight)
    {
      var newFooterMarginTop = height - mainHeight - footerHeight;
      $("main > footer").css({"margin-top" : newFooterMarginTop + "px"})
    }
  }
}

var privacyTextOpen = false;

function initCookies()
{
  if(Cookies.get('nitedelivery'))
  {
    $("#cookiesOverlay").remove();
  }
  else
  {
  console.log("Init");
    $("#cookiesOverlay").animate({"top":"0px"});
    $("#cookiesOverlay strong").click(clickOnOpenPrivacy);
    $("#acceptCookie").click(acceptCookieHandler);
  }
}

function acceptCookieHandler()
{
  if(privacyTextOpen)
    $("#cookiesOverlay").fadeOut(300,onCookieOverlayDisappear);
  else
  $("#cookiesOverlay").animate({"top" : "-100%"},1000,onCookieOverlayDisappear);
}

function onCookieOverlayDisappear()
{
  var thousandYears = 365*1000;
  Cookies.set('nitedelivery', 'privacy',{ expires: thousandYears });
  $("#cookiesOverlay").remove();
}

function clickOnOpenPrivacy()
{
  $("#cookiesOverlay .container-privacy").delay(600).slideDown(1500);
  $("#cookiesOverlay").animate({"height" : "100%"}, 1500, function()
    {
      privacyTextOpen = true;
      $("#cookiesOverlay").css({"overflow": "scroll"});
    });
}

function clickOnAcceptPrivacy()
{

}
var filters = [];
// var searchWord = ""
var filteredFound = [];
var headerSectionName = "";
var sectionName = "";
var autoChangeMenu = true;
var productsForSeach;

function setupMenu()
{
  $(".menu").on("click", ".view > div", clickOnChangeView);
  setupMenuAside();
  setupFilters();
  setupProductEvents();
  setupCarrello();
  $(document).scroll(onScrollHandler);
  // $(window).scroll(onScrollHandler);
  if(typeof(section) != "undefined"){
    setTimeout(function(){
      gotoSectionFromUrl(section);
    }, 200);
  }

  var mainScrollTop = $(document).scrollTop();
  setOverlayCarrelloMobile(mainScrollTop);
  initProductsForSeach();
  if(typeof(searchWord) != "undefined")
    updateFilters(searchWord);
}

function gotoSectionFromUrl(name)
{
  name = name.replace('&', '&amp;')
  var target = $(".center-menu").find('[data-main-section*="'+name+'"]');
  var element =  $(".aside-menu > section").find("> section li[data-section='"+name+"']").find("ul.list-group-in");
  autoChangeMenu = false;
  sectionName = element.find("li").eq(0).attr("data-section");
  openMainCategory(element);
  scrollToSecion(target);
}

function setupCarrello()
{
  $(".carrello-menu").on("click", ".product-remove", clickOnRemoveProduct);
  $(".carrello-menu").on("click", ".ordina", clickOnOrder);
}

function clickOnOrder()
{
  var url = $(".ordina").attr("data-checkout");
  window.location.href = url;
}

function setupProductEvents()
{
  $(".singleProduct").on("click", "a", clickOnAddSingleProduct);
  // $(".singleProduct").on("click", "a", clickOnAddSingleProduct);
  $(".center-menu").on("click", ".singleProduct .singleProduct-image", clickOnDetailsHandler);
  $(".center-menu").on("click", ".singleProduct .allergeniBtn", clickOnDetailsHandler);
  if(mobile)
    $(".center-menu").on("click", ".singleProduct .singleProduct-description", clickOnDetailsHandler);
  $(".overlay").on("click", "button", clickOnAddSingleProductFromOverlay);
}

function clickOnDetailsHandler()
{
  var product = $(this).parent().closest( ".singleProduct");
  setupOverlayDetails(product);
  getLineItemNotes(product);
  showOverlay();
}

function setupOverlayDetails(product)
{
  var id = product.attr("data-id");
  var img = product.find("img").attr("src");
  var name = product.find(".singleProduct-description.centerVertical h3").html();
  var description = product.find(".singleProduct-description-details").html();
  var prezzo = product.find(".infoForSquare .price-before-hour").html();
  var proporties = product.attr("class");

  proporties = proporties.replace('singleProduct', '');
  proporties = proporties.replace('filtered', '');
  if(proporties.indexOf("vegano") != -1)
    proporties = proporties.replace('vegetariano', '');
  proporties = proporties.trim();
  $(".overlay").find("img").attr("src", img);
  $(".overlay").find("h2.title").html(name);
  $(".overlay").find("p").html(description);
  $(".overlay").find(".price span").html(prezzo);
  $(".overlay").find(".properties").html(proporties);
  $(".overlay").find(".overlay-details").attr("data-id", id);
}

function clickOnAddSingleProductFromOverlay(e)
{
  hideOverlay();
  clickOnAddSingleProduct(e, $(this));
}

function clickOnRemoveProduct(e)
{
  e.stopPropagation();
  e.preventDefault();
  var variant_id = $(this).parent().attr("data-id");
  addOrRemoveSingleProduct(variant_id, -1000);
}

function clickOnAddSingleProduct(e, el)
{
  e.stopPropagation();
  e.preventDefault();
  var element = $(this).parent();
  if(typeof(el) != "undefined")
    element = el;
  var variant_id = element.closest("[data-id]").attr("data-id");
  var notes = element.closest("[data-id]").find("textarea").val();
  addOrRemoveSingleProduct(variant_id, 1, notes);
}

function addOrRemoveSingleProduct(variant_id, quantity, notes)
{
  notes = (typeof(notes) != "undefined" && notes != "") ? notes : ""
  var ajaxCall = $.ajax({ 
    url: '/orders/populate', 
    type: 'POST', 
    dataType: 'json', 
    data : {"variant_id" : variant_id, "quantity" : quantity, "description": notes}
  });
  ajaxCall.done(ajaxAddSuccess)
  .fail(ajaxAddError);
}

function ajaxAddSuccess(data, textStatus, jqXHR)
{
  var variantId = data["variant_id"];
  var already_present = ($(".carrello-menu li[data-id="+variantId+"]").length > 0) ? true: false;
  if(already_present)
  {
    if(data["variant_quantity"] > 0)
      updatePresentProduct(data);
    else
      removeProduct(data);
  }
  else
    addNewProduct(data);
  $(".carrello-menu .totale-prezzo").html("&euro; "+data["tot_price"]);
  $("#carrelloBtn > a").attr("data-total", data["tot_price"]);


  var newWidth = data["order_perc"]*100;
  diffOrderFloat = parseFloat(data["diff_order"].replace(",","."));
  $(".carrello-menu .minimoOrdine-background").css("width", newWidth + "%");
  if(diffOrderFloat > 0)
  {
    $(".carrello-menu .minimoOrdine-mancante").html(data["diff_order"]+" &euro; al minimo d'ordine");
    $(".minimoOrdine").removeClass("ordina");
  }
  else
  {
    $(".carrello-menu .minimoOrdine-mancante").html("Vai alla cassa");
    $(".minimoOrdine").addClass("ordina");
  }
  $(".navbar-default").find(".carrello-totProducts").html(data["tot_quantity"]);
}

function updatePresentProduct(data)
{
  $(".carrello-menu li[data-id="+data["variant_id"]+"]").find("span").html("x"+data["variant_quantity"]);
  var totPrice = data["variant_quantity"] * parseFloat(data["variant_price"].replace(',', '.'));
  totPrice = totPrice.toFixed(2).replace('.', ',');
  $(".carrello-menu li[data-id="+data["variant_id"]+"]").find(".product-price").html("&euro; " +totPrice);
}

function removeProduct(data)
{
  $(".carrello-menu section ul li[data-id="+data["variant_id"]+"]").slideUp(function()
    {
      $(this).remove();
    });;
}

function addNewProduct(data)
{
  var li = $(document.createElement("li")).addClass("clearFloat");
  li.attr("data-id", data["variant_id"]);
  var productName = $(document.createElement("div")).addClass("product-name");
  productName.html(data["variant_name"]+" <span class = \"product-quantity\">x1</span>");
  var productPrice = $(document.createElement("div")).addClass("product-price");
  productPrice.html("&euro; "+data["variant_price"]);
  li.append(productName);
  li.append(productPrice);
  li.append("<div class = \"product-remove\" data-toggle=\"tooltip\" title=\"Rimuovi dal carrello\">");
  $(".carrello-menu section ul").prepend(li);
  li.css({"display": "none"});
  li.slideDown();
  if(!mobile)
    $("li").find('[data-toggle="tooltip"]').tooltip(); 
}

function ajaxAddError(data, textStatus, jqXHR)
{  
  // console.log(data);
}

function onScrollHandler(event) {
    // var scroll = $(window).scrollTop();
    // var spaceLeft = $(".aside-menu").width();
    // $(".menu .center-menu").css({"left" : spaceLeft+"px"});
}

function setupFilters()
{
  $(".filters").on("click", "li", onClickFilters);
}


function initProductsForSeach()
{
  productsForSeach = [];
  var count = 0;
  $("section:not(.filtered)").find(".singleProduct").each(function()
    {
      var tempProduct = {"title" : "", "description" : "", "class" : "", "data-meta-keywords" : ""};
      tempProduct.title = $(this).find(".singleProduct-description > h3").html().toLowerCase();
      tempProduct.description = $(this).find(".singleProduct-description-details").html().toLowerCase();
      tempProduct.class = $(this).attr("class").toLowerCase();
      tempProduct["data-meta-keywords"] = $(this).attr("data-meta-keywords").toLowerCase();
      // if(tempProduct.title != "" || tempProduct.description != "" || tempProduct.class != "")
        productsForSeach.push(tempProduct)
    });
}

function onClickFilters()
{
  searchWord = "";
  $(this).toggleClass("filter-actived");
  updateFilters();
}

function updateFilters(search)
{
  if(typeof(search) != "undefined")
  {
    searchWord = search.toLowerCase();
    $(".filters").find(".filter-actived").removeClass("filter-actived");
  }
  updateFiltersArray(search);
  updateList();
  updateFilteredList();
  setNewFilteredHeight();
  if(filters.length > 0)
    scrollToSecion();
  if(filteredFound.length > 0)
    $("section.filtered").css({"opacity": 1, "margin-bottom" : "30px"});
  else if(filteredFound.length == 0 && filters.length > 0)
    $("section.filtered").css({"opacity": 1, "margin-bottom" : "30px"});
  else
    $("section.filtered").css({"opacity": 0, "margin-bottom" : "0px"});
  if(!mobile)
    $('.filtered [data-toggle="tooltip"]').tooltip(); 
}

function filterByWorld()
{

}

function updateList()
{
  $(".singleProduct").removeClass("filtered");
  var totFilters = filters.length;
  if(searchWord != "" || totFilters > 0)
  {
    var count = 0;
    $("section:not(.filtered)").find(".singleProduct").each(function()
    {
      var totFound = 0;
      for(var a = 0; a < totFilters; a++)
      {
        if($(this).hasClass(filters[a]))
          totFound++;
      }
      if(totFound == totFilters)
        $(this).addClass("filtered");
      else if(searchWord != "")
      {
        var words = [];
        words.push(searchWord);
        if(searchWord.indexOf(" ") != -1)
          words = searchWord.split(" ");
        for(var a = 0; a < words.length; a++)
        {
          if(productsForSeach[count].title.indexOf(words[a]) != -1 || productsForSeach[count].description.indexOf(words[a]) != -1 || productsForSeach[count].class.indexOf(words[a]) != -1 || productsForSeach[count]["data-meta-keywords"].indexOf(words[a]) != -1)
          {
            $(this).addClass("filtered");
          } 
        }
      }
      count++;
    });
  }
  filteredFound = [];
  $(".singleProduct.filtered").each(function()
  {
    filteredFound.push($(this).clone().removeClass("filtered"));
  });
}

function updateFilteredList()
{
  $("section.filtered").css("display", "none");
  $("section.filtered").find("li").remove();
  if(filteredFound.length > 0)
  {
    for(var a = 0; a < filteredFound.length; a++)
    {
      var element = filteredFound[a].appendTo("section.filtered ul").css("opacity", "0");
      var delay = 500 + (a/5 * 1000);
      element.animate({"opacity": 1}, delay);
    }
    $("section.filtered .singleProduct").on("click", "a", clickOnAddSingleProduct);
  }
  else
  {
    var noProductLi = document.createElement("li");
    $(noProductLi).html("Nessun prodotto trovato.").addClass("noFiltersFound");
    $("section.filtered ul").append(noProductLi);
  }
  $("section.filtered").css("display", "block");
}

function setNewFilteredHeight()
{
  var singleHeight = $(".singleProduct").height() + parseFloat($(".singleProduct").css("margin-bottom"));
  var totElement = filteredFound.length;
  var elementsForHeight = totElement;
  if($(".center-menu").hasClass("squareView"))
    elementsForHeight = Math.ceil(totElement/4);
  var newHeight = singleHeight * elementsForHeight;
  if(newHeight > 0)
    newHeight += 50;
  if(filters.length > 0 && totElement == 0)
    newHeight = 100;
  $("section.filtered").css("height", newHeight + "px");
}

function updateFiltersArray(search)
{
  filters = [];
  $(".filters").find(".filter-actived").each(function()
  {
    filters.push($(this).attr("data-filter"));
  });
  if(typeof(search) != "undefined")
    filters.push("fromSearch");
}

function setupMenuAside()
{
  // $(".aside-menu > section").on("click", "> section h2", toggleMenuSection);
  $(".aside-menu > section").on("click", "> section h3", clickOnCategoryName);
  $(".aside-menu > section").on("click", "> section .list-group-in .category-name", clickOnCategoryName);
  var allBlocks = $(".aside-menu > section > section ul.list-group-in");
  allBlocks.each(function()
  {
    $(this).css("height", $(this).height());
    $(this).attr("data-height", $(this).height());
  });
  setMenuCategoryNameTot();
  $(".overlayCarrelloMobile").on("click", clickonCarrelloMobile);
}

function clickonCarrelloMobile()
{
  var maxHeight = $(document).height();
  $('html, body').animate({scrollTop: maxHeight}, 600);
}

function clickOnCategoryName()
{
  var name = $(this).html();
  var target;
  var element;
  if($(this).hasClass("category-name"))
  {
    target = $(".center-menu").find('[data-section*="'+name+'"]');
    element = $(this).parent().parent();
    autoChangeMenu = false;
    sectionName = $(this).parent().attr("data-section");
  }
  else
  {
    target = $(".center-menu").find('[data-main-section*="'+name+'"]');
    element = $(this).parent().find("ul.list-group-in");
    autoChangeMenu = false;
    sectionName = element.find("li").eq(0).attr("data-section");
  }
  openMainCategory(element);
  scrollToSecion(target);
}

function setMenuCategoryNameTot()
{
  var all = $(".aside-menu .category-name-tot");
  var maxValue = 0;
  all.each(function(i)
  {
    var tot = $(this).html();
    if(maxValue < tot)
      maxValue = tot;
  });
  
  all.each(function(i)
  {
    var tot = $(this).html();
    var perc = tot/maxValue * 100;
    $(this).css("width", perc);
  });
}

function openMainCategory(element)
{
  var allBlocks = element;
  var prevActived = $("#taxonomies").find(".list-group > li.actived");
  $("#taxonomies").find("li").removeClass("actived");
  $(element).parent().addClass("actived");

  var section = $("#taxonomies").find("[data-section='"+sectionName+"']");
  $(section).addClass("actived");

  // var mainH2 = $(this);
  var opacity = 0;
  var actualHeight = $(element).height();
  var newHeight = 0;
  if(element.parent().hasClass("actived"))
  {
    var totElements = element.find("li").length;
    var liHeight = element.find("li").height() + 5;
    newHeight = totElements * liHeight ;
    opacity = 1;
  }
  if(prevActived)
    $(prevActived).find("ul.list-group-in").css({"opacity" : 0, "height": "0px"});
  element.css({"opacity" : 1, "height": newHeight + "px"});

  // $(this).parent().find(">ul").slideToggle({"duration" : 500, "easing" : "linear"});
  if(!autoChangeMenu)
  {
    setTimeout(function(){
      autoChangeMenu = true;
    }, 600);
  }
}

function clickOnChangeView()
{
  var myClass = $(this).attr("class");
  if(myClass == "linesIcon")
    activeLinesView();
  else
    activeSqureView();
  updateFilters();
}

function activeLinesView()
{
  $(".center-menu").addClass("linesView");
  $(".center-menu").removeClass("squareView");
  var height = $(".center-menu section li .singleProduct-image").width();
  $(".center-menu section li .singleProduct-image").css("height", height+"px");
}

function activeSqureView()
{
  $(".center-menu").addClass("squareView");
  $(".center-menu").removeClass("linesView");
  var height = $(".center-menu.squareView section li .singleProduct-image").width();
  $(".center-menu.squareView section li .singleProduct-image").css("height", height+"px");
}

function resizeMenu(width, height)
{
  $("aside-menu > section").css("height", height);
  $("body.menu > main > .container").css("display", "block");
  $("body.menu > main > footer").css("display", "block");
  resizeMenuAside(width, height);
}

function resizeMenuAside(width, height)
{
  var asideWidth;
  var asideCarrelloWidth;
  var leftAsideMenu;
  var leftAsideCarrello;
  var leftMenuCentralHeader;
  var centerMenuWidth = $(".center-menu").width();
  var mainContainerWidth = $("main>.container > .row").width();
  if(width >= 992)
  {
    asideWidth = mainContainerWidth / 100 * 16.66666667;
    leftAsideMenu = (width - mainContainerWidth)*.5;
    asideCarrelloWidth = mainContainerWidth / 100 * 16.66666667;
    leftAsideCarrello = ((width - mainContainerWidth)*.5);
    leftMenuCentralHeader = leftAsideMenu + asideWidth + 15;
  }
  else
  {
    asideCarrelloWidth = (mainContainerWidth / 100 * 25) - 10;
    leftAsideCarrello = 10 + ((width - mainContainerWidth)*.5);
  }
  $("#sidebar").css({"width" : asideWidth+"px", "left": leftAsideMenu+"px"});
  $(".carrello-menu").css({"width" : asideCarrelloWidth+"px", "right": leftAsideCarrello+"px"});
  $(".carrello-menu-header").css({"width" : (centerMenuWidth+2)+"px", "left": leftMenuCentralHeader+"px"});
  $(".center-menu").css({"left" : (asideWidth+2)+"px"});
}

function scrollToSecion(target)
{
  var offset = 0;
  if(target != undefined)
  {
    if(target.offset() != undefined)
      offset = target.offset().top  - 220;
    else
      offset = -100000;
  }
  if(offset >= 0)
    $('html, body').animate({scrollTop: offset}, 600);
}

function onScrollHandler()
{
  var mainScrollTop = $(document).scrollTop();
  checkScrollForAsideMenu();
  setOverlayCarrelloMobile(mainScrollTop);
}

function checkScrollForAsideMenu()
{
  var mainScrollTop = $(document).scrollTop();
  var newSectionName = sectionName;
  var newHeaderSectionName = headerSectionName;
  var minDistance = 1000000;
  var windowHeight = $(window).height();
  $(".center-menu").find("[data-section]:not([data-section=''])").each(function()
  {
    var offsetTop = $(this).offset().top;
    var diff = (220 - ((mainScrollTop - offsetTop)));

    var diffFromBottom = windowHeight - (offsetTop - mainScrollTop)
    var limitBottom = windowHeight / 10 * 3;
    if(diffFromBottom < minDistance && diffFromBottom > limitBottom)
    {
      minDistance = diffFromBottom;
      newSectionName = $(this).attr("data-section")
      var sectionName = $(this).attr("data-main-section")
      newHeaderSectionName = sectionName;
    }
    if(mainScrollTop < 200)
    {
      var element = $(".center-menu").find("[data-section]:not([data-section=''])").eq(0);
      newSectionName = $(element).attr("data-section")
      var sectionName = $(element).attr("data-main-section")
      newHeaderSectionName = sectionName;
    }
    if($(this).attr("data-section") == "Caffetteria")
    {
      var name =  $(this).attr("data-section");
      var diff = offsetTop - mainScrollTop;
      var diffFromBottom = $(window).height() - (offsetTop - mainScrollTop)
    }
  })

  if(newHeaderSectionName != headerSectionName)
  {
    headerSectionName = newHeaderSectionName;
    changeHeaderTitle(headerSectionName);
  }

  if(newSectionName != sectionName)
  {
    sectionName = newSectionName;
    if(sectionName != "")
      changeMenuFromScroll();
  }
}

function setOverlayCarrelloMobile(mainScrollTop)
{
  if(mobile)
  {
    var height = $(document).height();
    if(mainScrollTop < height - 1400)
      $(".overlayCarrelloMobile").css("bottom", "0px");
    else
      $(".overlayCarrelloMobile").css("bottom", "-200px");
  }
}

function changeMenuFromScroll()
{
  var section = $("#taxonomies").find("[data-section='"+sectionName+"']");
  var element = $(section).parent();
  if(autoChangeMenu)
    openMainCategory(element);   
}

function changeHeaderTitle(newTitle)
{
  $(".center-menu nav h2").stop(true, true).css("opacity", 0);
  $(".center-menu nav h2").html(newTitle);
  $(".center-menu nav h2").animate({"opacity" : 1}, 200);
}

function showOverlay()
{
  $(".overlay").css("left", "0%");
  $(".overlay").find("textarea").val("");
  $(".overlay").addClass("show");
  $(".overlay-close-btn").on("click", clickOnOverlayCloseBtn);

}

function getLineItemNotes(product)
{
  var variant_id = product.attr("data-id");
  var ajaxCall = $.ajax({ 
    url: '/orders/order_line_item', 
    type: 'POST', 
    dataType: 'json', 
    data : {"variant_id" : variant_id}
  });
  ajaxCall.done(ajaxAddSuccessOverlay)
  .fail(ajaxAddError);
}

function ajaxAddSuccessOverlay(data, textStatus, jqXHR)
{
  $(".overlay").find("textarea").val(data["notes"]);
}


function clickOnOverlayCloseBtn()
{
  $(".overlay-close-btn").off("click", clickOnOverlayCloseBtn);
  hideOverlay();
}

function hideOverlay()
{
  $(".overlay").removeClass("show");
  setTimeout(function()
  {
    $(".overlay").css("left", "100%");
  }, 1000)
}
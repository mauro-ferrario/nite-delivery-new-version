var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };
Spree.ready(function($) {
  Spree.onPayment = function() {
    if (($('#checkout_form_payment')).is('*')) {
      if (($('#existing_cards')).is('*')) {
        ($('#payment-method-fields')).hide();
        ($('#payment-methods')).hide();
        ($('#use_existing_card_yes')).click(function() {
          ($('#payment-method-fields')).hide();
          ($('#payment-methods')).hide();
          return ($('.existing-cc-radio')).prop("disabled", false);
        });
        ($('#use_existing_card_no')).click(function() {
          ($('#payment-method-fields')).show();
          ($('#payment-methods')).show();
          return ($('.existing-cc-radio')).prop("disabled", true);
        });
      }
      $(".cardNumber").payment('formatCardNumber');
      $(".cardExpiry").payment('formatCardExpiry');
      $(".cardCode").payment('formatCardCVC');
      $(".cardNumber").change(function() {
        return $(this).parent().siblings(".ccType").val($.payment.cardType(this.value));
      });
      ($('input[type="radio"][name="order[payments_attributes][][payment_method_id]"]')).click(function() {
        ($('#payment-methods li')).hide();
        if (this.checked) {
          return ($('#payment_method_' + this.value)).show();
        }
      });
      ($(document)).on('click', '#cvv_link', function(event) {
        var windowName, windowOptions;
        windowName = 'cvv_info';
        windowOptions = 'left=20,top=20,width=500,height=500,toolbar=0,resizable=0,scrollbars=1';
        window.open(($(this)).attr('href'), windowName, windowOptions);
        return event.preventDefault();
      });
      ($('input[type="radio"]:checked')).click();
      $('#coupon_code').focus(function(event){
        $("#checkout_form_payment input[type=submit]").prop("disabled", true);
      })
      $("#coupon_code.form-control").on("keydown", function(event) {
         if(event.which == 13) {
           $('#coupon_code').blur();
         }
       });
      $('#coupon_code').focusout(function(event) {
        var coupon_code, coupon_code_field, coupon_status, url;
        coupon_code_field = $('#coupon_code');
        coupon_code = $.trim(coupon_code_field.val());
        if (coupon_code !== '') {
          if ($('#coupon_status').length === 0) {
            coupon_status = $("<div id='coupon_status'></div>");
            coupon_code_field.parent().append(coupon_status);
          } else {
            coupon_status = $("#coupon_status");
          }
          url = Spree.url(Spree.routes.apply_coupon_code(Spree.current_order_id), {
            order_token: Spree.current_order_token,
            coupon_code: coupon_code
          });
          // coupon_status.removeClass();
          $.ajax({
            async: false,
            method: "PUT",
            url: url,
            success: __bind(function(data) {
              // event.preventDefault();
              $('#payment .field').addClass("disabled-step")
              coupon_status.addClass("alert-success").html("Coupon code applied successfully.");
              // submitPayment();
            }, this),
            error: function(xhr) {
              var handler;
              handler = JSON.parse(xhr.responseText);
              coupon_status.addClass("alert-error").html(handler["error"]);
              // Spree.enableSave();
              // event.preventDefault();
            }
          });
          $("#checkout_form_payment input[type=submit]").prop("disabled", false);
        } else {
          $("#checkout_form_payment input[type=submit]").prop("disabled", false);
        }
      return $('#checkout_form_payment').submit(function(event) {
        return true
      });
      });
    }
  };
  return Spree.onPayment();
});

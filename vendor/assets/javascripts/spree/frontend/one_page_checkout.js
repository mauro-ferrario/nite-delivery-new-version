var disable_steps, enable_step;
window.replace_checkout_step = function(step, partial, error) {
  disable_steps(true);
  if (partial != null) {
    step.html(partial);
  }
  if (!!error) {
    step.find('form.edit_order').prepend("<p class='checkout-error'>" + error + "</p>");
  }
  return enable_step(step);
};
enable_step = function(element) {
  element.removeClass("disabled-step");
  element.find("form input").removeAttr("disabled");
  element.find("#errorExplanation").html("Tutti i campi contrassegnati con l'asterisco devono essere compilati.");
  element.find("#checkout-summary, #errorExplanation").show();
  if (element.data('step') === 'address') {
    Spree.onAddress();
  }
  if (element.data('step') === 'payment') {
    return Spree.onPayment();
  }
    addToggleEvents();
};
disable_steps = function(all) {
  var elements;
  elements = all != null ? $(".checkout_content") : $(".checkout_content.disabled-step");
  elements.addClass("disabled-step");
  elements.find("form input").attr("disabled", "disabled");
    addToggleEvents();
  return elements.find("#checkout-summary, #errorExplanation").hide();
};
Spree.ready(function($) {
  if (($('#checkout')).is('*')) {
    return disable_steps();
  }
});


// var ajaxCallCheckout = $.ajax({
//     type: 'POST',
//     dataType: 'html'
//   });

// $("#checkout_form_payment").on("submit", submitPayment);
// $("#checkout_form_payment").submit(submitPayment);

function addToggleEvents(){
  var toggles = document.querySelectorAll('input[type="radio"]');
  for(var a = 0; a < toggles.length; a++){
    toggles[a].addEventListener('change', function(){
      checkToggle();
    })
  }
}

function checkToggle(){
  var paymentMethodField = document.querySelector('#payment-method-fields');
  var paymentByHandRadio = paymentMethodField.querySelectorAll('input')[0];
  var paymentByCardRadio = paymentMethodField.querySelectorAll('input')[1];
  var paymentMethodMobblie = document.querySelector('#payment_method_3');
  var paymentCreditCard= paymentMethodMobblie.querySelectorAll('input')[0];
  var paymentPayPall = paymentMethodMobblie.querySelectorAll('input')[1];
  var finalSubmitButton = document.querySelector('#checkout_form_payment .form-buttons input');
  var disabled = true;
  if(paymentByHandRadio.checked){
    disabled = false;
  }
  else{
    disabled = true;
    if(paymentCreditCard.checked || paymentPayPall.checked){
      disabled = false;
    }
  }
  if(disabled){
    finalSubmitButton.setAttribute("disabled","true");
  }
  else{
    finalSubmitButton.removeAttribute("disabled");
  }
}

// function submitPayment()
// {
  // e.preventDefault();
// }

$(document).on("ajax:success", "form", function(e, data, status, xhr)
{
  if(data["url"])
  {
    var host = window.location.protocol +"//"+ window.location.hostname;

    // if(host.indexOf("localhost") != -1)
      host += ":"+window.location.port;
    var url = host+data.url;
    window.location.href = url;
  }
});

document.addEventListener('DOMContentLoaded', onReadyCheckout);
function onReadyCheckout() {
 addToggleEvents();
}




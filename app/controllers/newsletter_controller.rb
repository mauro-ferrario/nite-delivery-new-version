class NewsletterController < ApplicationController
    respond_to :js, :json #, :only => :populate

    def add_to_newsletter
      #if params[:email] != "hello@mauroferrario.com"
        NewsletterMail.newsletter_email(params[:email]).deliver_now;
      #end
      #if params[:email] == "hello@mauroferrario.com"
        NewsletterNewUserMail.newsletter_email(params[:email]).deliver_now;
      #end

      respond_to do |format|
        # if !error
          format.json { render :json => {"test" => "ok", "email" => params[:email]}, :status => 200 } 
          format.html { render :nothing => true, :notice => 'Update SUCCESSFUL!' } 
        # else
        #   format.json { render :json => error, :status => 200 } 
        #   format.html { render :nothing => true, :notice => 'Update ERROR!' } 
        # end
      end

    end
  end

Spree::StaticContentController.class_eval do
  helper 'spree/products'
    respond_to :html

    def show
      # p "request.path = " + request.path
      @page = Spree::Page.by_store(current_store).visible.find_by!(slug: request.path)
      if @page.slug == "/menu" ||  @page.slug == "/promozioni" ||  @page.slug == "/chi-siamo" ||  @page.slug == "/faq" || @page.slug == "/termini-e-condizioni" || @page.slug == "/privacy" # Se sono nel menu
        @searcher = build_searcher(params.merge(include_images: true))
        @products = @searcher.retrieve_products
        @products = @products.includes(:possible_promotions) if @products.respond_to?(:includes)
        @taxonomies = Spree::Taxonomy.includes(root: :children)
        @order    = current_order(create_order_if_necessary: true)
        @total_order = @order.present? ? @order.total : 0.0
      end
    end
end 
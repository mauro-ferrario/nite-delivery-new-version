Spree::HomeController.class_eval do
   def index
      @searcher = build_searcher(params.merge(include_images: true))
      @products = @searcher.retrieve_products
      # Non capisco perchè devo prendere i prodotti
      @order    = current_order(create_order_if_necessary: false)
      @total_order = @order.present? ? @order.total : 0.0
    end
end 
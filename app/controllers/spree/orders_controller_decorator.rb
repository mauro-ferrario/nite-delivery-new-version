Spree::OrdersController.class_eval do
    include ActionView::Helpers::NumberHelper
     # Adds a new item to the order (creating a new order if none already exists)
    respond_to :js, :json #, :only => :populate

    def populate
      order    = current_order(create_order_if_necessary: true)
      variant  = Spree::Variant.find(params[:variant_id])
      quantity = params[:quantity].to_i
      options  = params[:options] || {}
      p "Description"
      p params[:description]

      # 2,147,483,647 is crazy. See issue #2695.
      if quantity.between?(-1000, 2_147_483_647)
        begin
          order.contents.add(variant, quantity, options)
        rescue ActiveRecord::RecordInvalid => e
          error = e.record.errors.full_messages.join(", ")
        end
      else
        error = Spree.t(:please_enter_reasonable_quantity)
      end

      line_item = order.find_line_item_by_variant(variant) 
      if line_item.quantity == 0
        line_item.destroy
      else
        line_item.description = params[:description] if params[:description].present?
        line_item.save!
      end
      diff_order = Rails.configuration.min_cost - order.total
      perc_diff_order =  (order.total == 0) ? 0 : order.total/Rails.configuration.min_cost
      perc_diff_order = 1 if perc_diff_order > 1
      diff_order = 0 if diff_order < 0
      respond_to do |format|
        if !error
          format.json { render :json => {"tot_quantity" => order.quantity,  "tot_price" => number_with_precision(order.total, precision: 2), "diff_order" => number_with_precision(diff_order, precision: 2), "order_perc" => perc_diff_order, "variant_name" => variant.name, "variant_id" => params[:variant_id], "variant_price" =>number_with_precision(line_item.price, precision: 2), "variant_quantity" => line_item.quantity}, :status => 200 } 
          format.html { render :nothing => true, :notice => 'Update SUCCESSFUL!' } 
        else
          format.json { render :json => error, :status => 200 } 
          format.html { render :nothing => true, :notice => 'Update ERROR!' } 
        end
      end
    end

    def order_line_item
      order    = current_order(create_order_if_necessary: true)
      variant  = Spree::Variant.find(params[:variant_id])
      line_item = order.find_line_item_by_variant(variant) 
      line_item_description = line_item.present? ? line_item[:description] : ""
      p line_item if line_item.present?
      p line_item[:description] if line_item.present?
      respond_to do |format|
        format.json { render :json => {"notes" => line_item_description}, :status => 200 } 
        format.html { render :nothing => true, :notice => 'Update SUCCESSFUL!' } 
      end
    end

    def update
      order = current_order
      line_item = Spree::LineItem.find(params[:variant_id])
      line_item.quantity += 10
      if line_item.quantity == 0
        line_item.destroy
      else
        line_item.save!
      end
      # order.update!
      diff_order = Rails.configuration.min_cost - order.total
      perc_diff_order =  (order.total == 0) ? 0 : order.total/Rails.configuration.min_cost
      perc_diff_order = 1 if perc_diff_order > 1
      diff_order = 0 if diff_order < 0

      respond_to do |format|
        format.json { render :json => {"tot_quantity" => params[:variant_id],  "tot_price" => order.total, "diff_order" => diff_order, "order_perc" => perc_diff_order, "variant_name" => variant.name, "variant_id" => params[:variant_id], "variant_price" =>line_item.price, "variant_quantity" => line_item.quantity}, :status => 200 } 
        format.html { render :nothing => true, :notice => 'Update SUCCESSFUL!' } 
        # format.json{render json: @order}
      end
    end
end 
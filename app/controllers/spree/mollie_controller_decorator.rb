Spree::MollieController.class_eval do
    before_action :flash_from_mollie

    def flash_from_mollie
      flash[:from_mollie_payment] = true
    end

end

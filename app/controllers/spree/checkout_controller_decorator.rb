Spree::CheckoutController.class_eval do
  before_action :set_error_if_payment_refused

   def update
    if @order.update_from_params(params, permitted_checkout_attributes, request.headers.env)
      @order.temporary_address = !params[:save_user_address]
      unless @order.next
        flash[:error] = @order.errors.full_messages.join("\n")
        p @order.errors.full_messages.join("\n")
        redirect_to(checkout_state_path(@order.state)) && return
      end

      if @order.completed?
        @current_order = nil
        flash.notice = Spree.t(:order_processed_successfully)
        flash['order_completed'] = true
        # OrderMail.send_order_info("info@nitedelivery.com", @order.number).deliver_now;
        respond_to do |format|
          format.json { render :json => {"url" => completion_route}, :status => 200, :turbolinks => false }
          format.html { redirect_to completion_route(@order) }
          # format.html { render :nothing => true, :notice => 'Update SUCCESSFUL!' }
        end
      else
        redirect_to checkout_state_path(@order.state)
      end
    else
      render :edit
    end
  end

  private

  def set_error_if_payment_refused
    if flash[:from_mollie_payment]
      @refused_payment = @order.payments.last
    end
  end

end

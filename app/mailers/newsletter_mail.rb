class NewsletterMail < ApplicationMailer
  default from: 'info@nitedelivery.com'
 
  def newsletter_email(email)
    @email = email;
    mail(to: "info@nitedelivery.com", subject: 'New user for newsletter')
  end
end

class NewsletterNewUserMail < ApplicationMailer
  default from: 'info@nitedelivery.com'
 
  def newsletter_email(email)
    @email = email;
    mail(to: @email, subject: 'Conferma iscrizione alla newsletter di Nite Delivery')
  end
end

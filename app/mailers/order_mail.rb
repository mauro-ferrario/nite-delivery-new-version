class OrderMail < ApplicationMailer
  default from: 'info@nitedelivery.com'
 
  def send_order_info(email, number)
    @email = email;
    @number = number;
    mail(to: @email, subject: 'Nuovo ordine')
  end
end

module Spree
  Variant.class_eval do
    alias_method :orig_price_in, :price_in

    def price_in(currency)
      t = Time.now
      hour = t.strftime('%k')
      return orig_price_in(currency) unless (hour.to_i >= Rails.configuration.hour_for_price)
      price_by_hour = get_amount_by_hour(currency)
      Spree::Price.new(variant_id: self.id, amount: price_by_hour, currency: currency)
    end


    def get_price_number(currency)
      t = Time.now
      hour = t.strftime('%k')
      if hour.to_i >= Rails.configuration.hour_for_price
        if self.price_by_hour.present?
          price = self.price_by_hour
        elsif
          price = self.price + 1.0
        end
      elsif
        price = self.price
      end
      price
    end

    def get_normal_amount(currency)
      orig_price_in(currency).amount
    end

    def get_amount_by_hour(currency)
      p = Spree::Price.new(variant_id: self.id, amount: self.price_by_hour, currency: currency)
      if p.amount.present?
        amount = p.amount
      elsif
        origin_price = get_normal_amount(currency) + 1.0
        amount = origin_price
      end
      amount
    end
  end
end
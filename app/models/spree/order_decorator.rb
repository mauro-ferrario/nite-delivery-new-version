Spree::Order.class_eval do

     checkout_flow do
      go_to_state :address
      go_to_state :payment, if: ->(order) { order.payment_required? }
      # go_to_state :confirm, if: ->(order) { order.confirmation_required? }
      go_to_state :complete
      remove_transition from: :delivery, to: :confirm
    end


end


Spree::Address.class_eval do
  def require_state?
      false
  end

  def require_country?
      false
  end
end


Spree.config do |config|
  config.address_requires_state = false
  config.address_requires_state = false
end  

# Spree::Billing.class_eval do
#   def require_zipcode?
#       false
#   end
# end
module Spree
  Product.class_eval do
    delegate :price_by_hour, :price_by_hour=, :dish_of_the_week, :dish_of_the_week=, :dish_of_the_week_description, :dish_of_the_week_description=, to: :master
  end 
end
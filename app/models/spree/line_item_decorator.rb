Spree::LineItem.class_eval do
  def copy_price
    if variant
      self.price = variant.get_price_number("EUR")
      self.cost_price = variant.cost_price if cost_price.nil?
      self.currency = variant.currency if currency.nil?
    end
  end
end
Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'add_price_by_hoursto_product_edit',
  insert_after: "erb[loud]:contains('text_field :price')",
  text: "
    <%= f.field_container :price_by_hour do %>
      <%= f.label :price_by_hour, 'Prezo dopo le ore '+ Rails.configuration.hour_for_price.to_s + ' (default + 1)' %>
      <%= f.text_field :price_by_hour, value:
        number_to_currency(@product.price_by_hour, unit: '') %>
      <%= f.error_message_on :price_by_hour %>
    <% end %>
  ")
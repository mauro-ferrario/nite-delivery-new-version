Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'add_dish_of_the_week_description_to_product_edit',
  insert_after: "[data-hook=admin_product_form_description]",
  text: "
  <div data-hook=\"admin_product_form_dish_of_the_week_escription\">
        <%= f.field_container :dish_of_the_week_description, class: ['form-group'] do %>
          <%= f.label :dish_of_the_week_description, 'Descrizione piatto della settimana' %>
          <%= f.text_area :dish_of_the_week_description, { rows: \"20\", class: 'form-control' } %>
          <%= f.error_message_on :description %>
        <% end %>
      </div>")
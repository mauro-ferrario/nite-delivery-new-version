Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'remove_cost_price',
  remove: '[data-hook=admin_product_form_cost_price]')

Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'remove_cost_currency',
  remove: '[data-hook=admin_product_form_cost_currency]')

Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'remove_discontinue_on',
  remove: '[data-hook=admin_product_form_discontinue_on]')

Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'remove_promotionable',
  remove: '[data-hook=admin_product_form_promotionable]')

Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'remove_shipping_specs',
  remove: '#shipping_specs')

Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'remove_option_types',
  remove: '[data-hook=admin_product_form_option_types]')

Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'remove_meta_title',
  remove: '[data-hook=admin_product_form_meta_title]')

# Deface::Override.new(virtual_path: 'spree/admin/products/_form',
#   name: 'remove__meta_keywords',
#   remove: '[data-hook=admin_product_form_meta_keywords]')

Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'remove__meta_description',
  remove: '[data-hook=admin_product_form_meta_description]')


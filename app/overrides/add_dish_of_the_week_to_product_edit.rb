Deface::Override.new(virtual_path: 'spree/admin/products/_form',
  name: 'add_dish_of_the_week_to_product_edit',
  insert_before: "[data-hook=admin_product_form_available_on]",
  text: "
    <%= f.field_container :dish_of_the_week do %>
      <%= f.label :dish_of_the_week, 'Piatto della settimana' %>
      <%= f.check_box :dish_of_the_week, value:
        @product.dish_of_the_week %>
      <%= f.error_message_on :dish_of_the_week %>
    <% end %>
  ")
Spree::BaseHelper.class_eval do
    def taxons_tree(root_taxon, current_taxon, max_level = 1)   # Sovrascritto dall'originale
      return '' if max_level < 1 || root_taxon.leaf?
      _class = "list-group"
      _class = "list-group-in" if root_taxon.depth == 1
      content_tag :ul, class: _class do
          taxons = root_taxon.children.map do |taxon|
            li_class = ""
            li_class = "noPointer" if taxon.products.size.to_s == 0
            content_tag :li, class: li_class, "data-section": taxon.name do
            str = ""
            css_class = (current_taxon && current_taxon.self_and_ancestors.include?(taxon)) ? 'list-group-item active' : 'list-group-item'
            if taxon.depth == 1
              str += content_tag :h3 do
                taxon.name
              end
            end
            if taxon.depth == 2
              str += content_tag :div, class: 'category-name' do
                taxon.name
              end
              str += content_tag :div, class: 'category-name-tot' do
                p taxon
                taxon.products.size.to_s
              end
            end
            ##if taxon.name != "Primi"
              str.html_safe + taxons_tree(taxon, current_taxon, max_level - 1).html_safe
            ##end
          end
        end
        safe_join(taxons, "\n")
      end
    end

    def get_dish_of_the_week
       Spree::Product.joins(:variants_including_master).where('spree_variants.dish_of_the_week = ?', true)
    end


    def body_class page = nil # Sovrascritto dall'originale
       #return "tempPage" if params[:admin].nil? and spree_current_user.nil?
       if (controller_name == "orders" and action_name == "show")
         OrderMail.send_order_info("info@nitedelivery.com", @order.number).deliver_now;
       end
       return "order" if controller_name == "orders" and action_name == "show"
       return "login" if controller_name == "user_passwords" and action_name == "new"
       return "login" if controller_name == "user_registrations" and action_name == "new"
       return "login" if controller_name == "user_sessions"
       return "checkout" if controller_name == "checkout" and action_name == "edit"
       return "registration" if controller_name == "checkout" and action_name == "registration"
       return "registration" if controller_name == "checkout" and action_name == "update_registration"
       return "cart empty_cart" if controller_name == "orders" and action_name == "edit" && @order.line_items.empty?
       return "cart" if controller_name == "orders" and action_name == "edit"
       return "home" if controller_name == "home" and action_name == "index"
       return "menu" if controller_name == "static_content" and action_name == "show" and page.slug == "/menu"
       return "promozioni" if controller_name == "static_content" and action_name == "show" and page.slug == "/promozioni"
       return "chi-siamo" if controller_name == "static_content" and action_name == "show" and page.slug == "/chi-siamo"
       return "faq" if controller_name == "static_content" and action_name == "show" and page.slug == "/faq"
       return "terminiECondizioni" if controller_name == "static_content" and action_name == "show" and page.slug == "/termini-e-condizioni"
       return "terminiECondizioni" if controller_name == "static_content" and action_name == "show" and page.slug == "/privacy"
    end

end
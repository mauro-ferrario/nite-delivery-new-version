# This migration comes from spree_price_by_hours (originally 20180407113618)
class AddPriceByHoursToSpreeVariants < ActiveRecord::Migration[5.1]
  def change
  	add_column :spree_variants, :price_by_hour, :decimal, precision: 8, scale: 2
  end
end

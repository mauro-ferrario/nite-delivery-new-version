# This migration comes from spree_add_line_item_description (originally 20180407114855)
class AddDescriptionToSpreeLineItems < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_line_items, :description, :text
  end
end

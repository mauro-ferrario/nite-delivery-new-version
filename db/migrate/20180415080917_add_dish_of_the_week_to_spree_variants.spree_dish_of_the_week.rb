# This migration comes from spree_dish_of_the_week (originally 20180407114606)
class AddDishOfTheWeekToSpreeVariants < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_variants, :dish_of_the_week, :boolean
    add_column :spree_variants, :dish_of_the_week_description, :text
  end
end
